import { RouterModule, Routes } from '@angular/router';
import { AdditionalFeaturesComponent } from './additional-features/additional-features.component';
import { HomeComponent } from './home/home.component';
import { PlanCardComponent } from './plan-card/plan-card.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'planUpdateCard',
    component: PlanCardComponent
  },
  {
    path: 'additionalFeatures',
    component: AdditionalFeaturesComponent
  }
];

export const appRoutes = RouterModule.forRoot(routes);
