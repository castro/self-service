import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-additional-features',
  templateUrl: './additional-features.component.html',
  styleUrls: ['./additional-features.component.scss']
})
export class AdditionalFeaturesComponent implements OnInit {

  active: Boolean = false;
  additionalFeatures = [
    {
      description: 'All Smart Tools'
    }, {
      description: 'Videos in HD and Full HD'
    }, {
      description: 'Special condition for lead prospecting / new students'
    }, {
      description: 'SCORM Content'
    }
  ];
  features = [
    {
      id: 0,
      name: 'Funcionalidade',
      active: false,
      price: '18,00',
      // tslint:disable-next-line:max-line-length
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
      icon: 'fas fa-dumbbell fa-2x',
      type: 'gym'
    }, {
      id: 1,
      name: 'Funcionalidade',
      active: false,
      price: '15,99',
      // tslint:disable-next-line:max-line-length
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
      icon: 'fas fa-shield-alt fa-2x',
      type: 'default'
    }, {
      id: 2,
      name: 'Funcionalidade',
      active: true,
      price: '18,00',
      // tslint:disable-next-line:max-line-length
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
      icon: 'fas fa-cloud fa-2x',
      type: 'cloud'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

  changeActive(value): void {
    this.active = value;
  }

}
