import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { appRoutes } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { PlanCardComponent } from './plan-card/plan-card.component';
import { AdditionalFeaturesComponent } from './additional-features/additional-features.component';
import { StepsBarComponent } from './steps-bar/steps-bar.component';

@NgModule({
  declarations: [
    AppComponent,
    PlanCardComponent,
    HomeComponent,
    AdditionalFeaturesComponent,
    StepsBarComponent
  ],
  imports: [
    BrowserModule,
    appRoutes,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
