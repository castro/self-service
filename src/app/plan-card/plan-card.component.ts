import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-plan-card',
  templateUrl: './plan-card.component.html',
  styleUrls: ['./plan-card.component.scss']
})
export class PlanCardComponent implements OnInit {

  features = [
    {
      description: 'Customized Layout/Landing Page'
    }, {
      description: 'E-commerce and Integrated Payment System'
    }, {
      description: 'Unlimited courses, Disk space Traffic'
    }
  ];

  additionalFeatures = [
    {
      description: 'Customized Layout/Landing Page'
    }, {
      description: 'E-commerce and Integrated Payment System'
    }, {
      description: 'Unlimited courses, Disk space Traffic'
    }, {
      description: 'Custom domain'
    }, {
      description: 'Unlimited users in Live Streaming (Webinar)'
    }, {
      description: 'Unlimited users in free lecture for paid courses'
    }, {
      description: 'Up to 50 active users included at no additional cost'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
